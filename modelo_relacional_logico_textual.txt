Pessoa(_ID, Nome)
Curriculo(_ID, Abstract, Produtividade, #Departamento, Sala, Telefone, #Pessoa, site)
BolsistaCurriculo(_#Universidade, _#Curriculo)
NomeCitacao(_ID, #Curriculo, Citacao)

Departamento(_ID, Nome, #Campus)
Campus(_ID, Nome, #CEP, Numero, #Universidade)
Universidade(_ID, Nome, #CampusPrincipal)

CEP(_ID, CEP, #Rua, #Bairro)
Rua(_ID, Nome)
Bairro(_ID, Nome, #Cidade)
Cidade(_ID, Nome, #Estado)
Estado(_ID, Nome, #Pais)
Pais(_ID, Nome)

FormacaoAcademica(_ID, #Curriculo, Titulo, DataInicio, DataFim, #Orientador, #TipoFormacao, #Curso, capes)
TipoFormacao(_ID, Tipo)
Curso(_ID, Nome)
FormacaoAcademicaBolsista(_#FormacaoAcademica, _#Orgao)
FormacaoAcademicaUniversidade(_#FormacaoAcademica, _#Universidade)
FormacaoAcademicaGrandeArea(_#FormacaoAcademica, _#GrandeArea, #Area, #SubArea, #Especialidade)

PosDoutorado(_ID, #Curriculo, DataInicio, DataFim)
PosDoutoradoUniversidade(_#PosDoutorado, _#Universidade)
PosDoutoradoBolsista(_#PosDoutorado, _#Orgao)
PosDoutoradoGrandeArea(_#PosDoutorado, _#GrandeArea, #Area, #SubArea, #Especalidade)

Orgao(_ID, Nome)

GrandeArea(_ID, Nome)
Area(_ID, Nome, #GrandeArea)
SubArea(_ID, Nome, #Area)
Especializacao(_ID, Nome, #SubArea)

FormacaoComplementar(_ID, #Curriculo, DataInicio, DataFim, Descrição, CargaHoraria, #Instituicao)

Vinculo(_ID, #Curriculo, #Instituicao,  DataInicio, DataFim, Enquadramento, Exclusividade)
Atividade(_ID, #Vinculo, DataInicio, DataFim, Descricao, Tipo)

Instituicao(_ID, Nome, #CEP, Numero)

LinhaDePesquisa(_ID, #Curriculo, Descricao)

Projeto(_ID, #Curriculo, Descricao, DataInicio, DataFim, Titulo, Concluido, Tipo, #Financiador, NProducoes, NOrientacoes, NAlunosDoutorado, NAlunosGraduacao, NAlunosMestrado)
ProjetoIntegrantes(_#Projeto, _#Integrante, #Cargo)
Cargo(_ID, Descricao);

RevisaoProjeto(_ID, #Curriculo, Tipo, DataInicio, DataFim, Descricao)

Idiomas(_ID, Lingua)
FalaIdiomas(_#Idioma, _#Curriculo, Escrita, Leitura, Fala, Compreensao)

Premios(_ID, #Curriculo, DataPublicacao, Descricao)

Citacoes(_#NomeCitacao, _#Plataforma, TotalCitacoes, TotalTrabalhos, FatorH)
Plataforma(_ID, Nome)

Artigo(_ID, Titulo, #Convencao, Volume, Paginas, DataPublicacao)
ArtigoAutor(_#Artigo, _#Pessoa)
ArtigoCitacao(_#Artigo, _#Plataforma, Quantidade)

Livro(_ID, Titulo, Edicao, #Instituicao, NPaginas, DataPublicacao)
LivroAutor(_#Livro, _#Pessoa)

CapituloLivro(_ID, TituloCapitulo, #Livro, NPaginas)
CapituloLivroAutor(_#CapituloLivro, _#Pessoa)

Texto(_ID, Titulo, #Convencao, Paginas, DataPublicacao)
TextoAutor(_#Texto, _#Pessoa)

Convencao(_ID, Nome, Sigla)

TrabalhoCongresso(_ID, Titulo, #Convencao, DataPublicacao, Paginas)
TrabalhoCongressoAutor(_#TrabalhoCongresso, _#Pessoa)

ResumoCongressos(_ID, Titulo, #Convencao, DataPublicacao, Paginas)
ResumoCongressoAutor(_#ResumoCongresso, _#Pessoa)

Apresentacao(_ID, Titulo, DataPublicacao)
ApresentacaoAutor(_#Resumo, _#Pessoa)

Outros(_ID, Descricao, DataPublicacao, Tipo)
OutrosAutor(_#Outros, _#Pessoa)

TrabalhoTecnico(_ID, Descricao, DataPublicacao)
TrabalhoTecnicoAutor(_#TrabalhoTecnico, _#Pessoa)

Comissao(_ID, #TipoComissao, #Universidade)
ComissaoParticipante(_#Comissao, _#Pessoa)
TipoComissao(_ID, Descricao)

Banca(_ID, #TipoBanca, Titulo, #Avaliado, #Universidade)
BancaParticipante(_#Banca, _#Pessoa)
TipoBanca(_ID, Descricao)

Evento(_ID, Nome, Sigla, Tipo)
ParticipacaoEvento(_#Evento, _#Curriculo)

Orientacao(_#Curriculo, _#Pessoa, Tipo, Titulo, AnoInicio)
